/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phystem.bot;

/**
 *
 * @author Madhanraj
 */
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class LogUtil {

    public static Logger getLogger(String className) {
        init();
        Logger log = Logger.getLogger(className);
        log.addHandler(handler);
        return log;
    }

    private final static Logger LOG = Logger.getLogger(LogUtil.class.getName());
    private static FileHandler handler;

    static void init() {
        try {
            if (handler == null) {
                new File("logs").mkdirs();
                handler = new FileHandler("logs" + File.separator + "log-%g.txt", 4096 * 1024, 100, true);
                handler.setFormatter(new VerySimpleFormatter());
                LOG.addHandler(handler);
            }
        } catch (IOException | SecurityException ex) {
            LOG.log(Level.SEVERE, "Error creating log", ex);
        }
    }
}

class VerySimpleFormatter extends Formatter {

    private static final String PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    @Override
    public String format(final LogRecord record) {
        return String.format(
                "%1$s %2$-7s %3$s\n",
                new SimpleDateFormat(PATTERN).format(
                        new Date(record.getMillis())),
                record.getLevel().getName(), formatMessage(record));
    }

}
