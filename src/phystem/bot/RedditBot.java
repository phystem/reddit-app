/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phystem.bot;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import phystem.model.UserData;

/**
 *
 * @author Madhanraj
 */
public class RedditBot {

    private static final Logger LOG = LogUtil.getLogger("RedditBot");

    UserData user;

    Boolean isPost;

    Boolean isUpVote;

    String url;

    String titleToSearch;

    String commentStartsWith;

    int randomTimesDef;

    int randomTimes;

    int actionWaitTime;

    WebDriver driver;

    WebDriverWait wait;

    public RedditBot(Boolean isPost, Boolean isUpVote, String url, String titleToSearch, String commentStartsWith, int[] randomTime) {
        LOG.info("===================================");
        this.isPost = isPost;
        this.isUpVote = isUpVote;
        this.url = url;
        this.titleToSearch = titleToSearch;
        this.commentStartsWith = commentStartsWith;
        this.randomTimesDef = randomOf(randomTime[0], randomTime[1]);
        LOG.log(Level.INFO, "Configuring Bot for [{0}] [{1}] with Random Upvote of [{2}] times",
                new Object[]{
                    isUpVote ? "UpVoting" : "DownVoting",
                    isPost ? "Post" : "Comment",
                    randomTimes});
    }

    public void start(UserData user) {
        LOG.log(Level.INFO, "Starting With user [{0}]", user.username);
        this.randomTimes = randomTimesDef;
        this.actionWaitTime = randomOf(1, user.waitTime);

        this.user = user;
        user.status = "";
        try {
            createDriver();
            if (proxyWorking() && login()) {
                if (isPost) {
                    randomUpVotePost();
                } else {
                    popularUpVoteComment();
                }
                user.status = upVoteOrDownVote() ? "Success" : "Could not Vote";
//                logout();
            }
        } catch (Exception ex) {
            user.status = "Run Failed";
            LOG.log(Level.SEVERE, null, ex);
        }
        stop();
    }

    private void createDriver() {
        LOG.log(Level.INFO, "Creating Driver with Proxy [{0}]", user.proxy);
        LOG.log(Level.INFO, "Creating Driver with UserAgent [{0}]", user.uAgent);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        Proxy proxy = new Proxy();
        proxy.setHttpProxy(user.proxy).setSslProxy(user.proxy).setFtpProxy(user.proxy);
        capabilities.setCapability("proxy", proxy);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--user-agent=" + user.uAgent);
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        driver = new ChromeDriver(capabilities);
        wait = new WebDriverWait(driver, 60);

        LOG.info("Driver Created");
    }

    private Boolean proxyWorking() {
        driver.get("https://www.freecodecamp.com/the-fastest-web-page-on-the-internet");
        Boolean connected = driver.findElements(By.xpath("//*[text()='There is no Internet connection' or text()='This site can’t be reached']")).isEmpty();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (!connected) {
            LOG.severe("Proxy Not Connected");
            user.status = "Proxy Failure";
        } else {
            LOG.severe("Proxy Connected");
        }
        return connected;
    }

    private Boolean login() {
        LOG.log(Level.INFO, "Logging in [{0}]", user.username);

        driver.get("https://www.reddit.com/login");
        waitGet(By.id("user_login")).sendKeys(user.username);
        get(By.id("passwd_login")).sendKeys(user.password);
        get(By.cssSelector("#login-form button[type='submit']")).click();

        Boolean loggedIn = isLoggedIn();
        if (loggedIn) {
            LOG.info("Logged in");
        } else {
            LOG.info("Login Failed");
            user.status = "Login Failure";
        }
        return loggedIn;
    }

    private Boolean isLoggedIn() {
        sleep(3);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOfElementLocated(By.className("c-form-throbber")));
        Boolean loggedIn = driver.findElements(By.cssSelector(".c-form-group.c-has-error")).isEmpty();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return loggedIn;

    }

    private void randomUpVotePost() {
        LOG.log(Level.INFO, "Random upvoting/downvoting Posts for {0} times", randomTimes);

        while (randomTimes-- > 0) {
            userSleep();
            waitGet(By.xpath("//a[contains(@class,'choice')][text()='random']")).click();
            randomScroll();
            if (isPostNotArchived()) {
                String upRdown = randomOf(0, 1) == 0 ? "up" : "down";
                randomClickFromList(By.cssSelector("#siteTable .arrow." + upRdown));
                LOG.log(Level.INFO, "Random Upvote/Downvote {0} done", randomTimes + 1);
            } else {
                LOG.info("Skipping coz of Archived Post");
            }
        }
    }

    private void popularUpVoteComment() {
        LOG.log(Level.INFO, "Popular upvoting Comments for {0} times", randomTimes);

        userSleep();
        waitGet(By.xpath("//a[contains(@class,'choice')][text()='popular']")).click();

        randomClickFromList(By.cssSelector("#siteTable .comments"));

        sortByNew();
        if (isPostNotArchived()) {
            while (randomTimes-- > 0) {
                randomScroll();
                String upRdown = randomOf(0, 1) == 0 ? "up" : "down";
                randomClickFromList(By.cssSelector(".commentarea .arrow." + upRdown));
                LOG.log(Level.INFO, "Popular Upvote/DownVote {0} done", randomTimes + 1);
            }
        } else {
            LOG.info("Skipping coz of Archived Post");
        }
    }

    private void sortByNew() {
        LOG.info("Sorting By New Comments");
        waitGet(By.cssSelector(".menuarea .dropdown.lightdrop")).click();
        waitGet(By.xpath("//div[contains(@class,'drop-choices lightdrop')]/a[text()='new']")).click();
    }

    private Boolean upVoteOrDownVote() {
        LOG.log(Level.INFO, "Going to Target Url {0}", url);
        driver.get(url);
        sleep(3);
        if (isPostNotArchived()) {
            String index = findPostIndexByTitle();
            if (index != null) {
                if (isPost) {
                    if (isUpVote) {
                        upVotePost(index);
                    } else {
                        downVotePost(index);
                    }
                } else {
                    goToCommentPage();
                    userSleep();
                    String commentIndex = getCommentIndex();
                    if (commentIndex != null) {
                        if (isUpVote) {
                            upVoteComment(commentIndex);
                        } else {
                            downVoteComment(commentIndex);
                        }
                    } else {
                        LOG.info("Could not Find Comment with given Text");
                        return false;
                    }
                }
                userSleep();
                return true;
            } else {
                LOG.info("Could not Find Post with given Title");
                return false;
            }
        } else {
            LOG.warning("Post is archived Can't Vote");
            return false;
        }
    }

    private String findPostIndexByTitle() {
        LOG.info("Finding Post with given Title");
        List<WebElement> posts
                = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("#siteTable .entry a.title")));
        for (WebElement post : posts) {
            if (post.getText().equals(titleToSearch)) {
                return String.valueOf(posts.indexOf(post) + 1);
            }
        }
        return null;
    }

    private void upVotePost(String index) {
        LOG.info("UpVoting Post");
        WebElement element = waitGet(By.xpath("(//div[@id='siteTable']//div[@data-event-action='upvote'])[" + index + "]"));
        if (element.getAttribute("class").contains("upmod")) {
            LOG.info("Post Already Upvoted");
        } else {
            for (int i = 0; i < 3; i++) {
                element.click();
                sleep(1);
            }
            LOG.info("Post Upvoting Done");
        }
    }

    private void downVotePost(String index) {
        LOG.info("DownVoting Post");
        WebElement element = waitGet(By.xpath("(//div[@id='siteTable']//div[@data-event-action='downvote'])[" + index + "]"));
        if (element.getAttribute("class").contains("downmod")) {
            LOG.info("Post Already DownVoted");
        } else {
            for (int i = 0; i < 3; i++) {
                element.click();
                sleep(1);
            }
            LOG.info("Post DownVoting Done");
        }
    }

    private void goToCommentPage() {
        List<WebElement> posts = driver.findElements(By.cssSelector(".entry"));
        for (WebElement post : posts) {
            WebElement title = post.findElement(By.xpath(".//a[contains(@class,'title')]"));
            if (title.getText().equals(titleToSearch)) {
                post.findElement(By.xpath(".//a[@data-event-action='comments']")).click();
                break;
            }
        }
    }

    private String getCommentIndex() {
        LOG.info("Finding Comment with given Comment Text");
        List<WebElement> comments
                = driver.findElements(By.cssSelector(".commentarea .usertext-body .md>p:nth-of-type(1)"));
        for (WebElement comment : comments) {
            if (comment.getText().startsWith(commentStartsWith)) {
                return String.valueOf(comments.indexOf(comment) + 1);
            }
        }
        return null;
    }

    private void upVoteComment(String index) {
        LOG.info("UpVoting Comment");
        WebElement element = waitGet(By.xpath("(//div[@class='commentarea']//div[@data-event-action='upvote'])[" + index + "]"));
        if (element.getAttribute("class").contains("upmod")) {
            LOG.info("Comment Already Upvoted");
        } else {
            for (int i = 0; i < 3; i++) {
                element.click();
                sleep(1);
            }
            LOG.info("Comment Upvoting Done");
        }
    }

    private void downVoteComment(String index) {
        LOG.info("DownVoting Comment");
        WebElement element = waitGet(By.xpath("(//div[@class='commentarea']//div[@data-event-action='downvote'])[" + index + "]"));
        if (element.getAttribute("class").contains("downmod")) {
            LOG.info("Comment Already DownVoted");
        } else {
            for (int i = 0; i < 3; i++) {
                element.click();
                sleep(1);
            }
            LOG.info("Comment DownVoting Done");
        }
    }

    private Boolean isPostNotArchived() {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        Boolean notarchived = driver.findElements(By.cssSelector(".content .archived-infobar")).isEmpty();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return notarchived;
    }

    private void randomClickFromList(By by) {
        List<WebElement> elements = driver.findElements(by);
        if (!elements.isEmpty()) {
            elements.get(randomOf(0, elements.size() - 1)).click();
        } else {
            LOG.info("Voting Disabled in this section");
        }
    }

    private void randomScroll() {
        LOG.info("Scroll Like User");
        int times = randomOf(3, 6);

        String docScrollHeight = "Math.max("
                + "document.body.scrollHeight, document.documentElement.scrollHeight,"
                + "document.body.offsetHeight, document.documentElement.offsetHeight,"
                + "document.body.clientHeight, document.documentElement.clientHeight)";

        int maxHeight = Integer.valueOf(((JavascriptExecutor) driver).executeScript("return " + docScrollHeight).toString());

        while (times-- > 0) {
            ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, " + randomOf(200, maxHeight) + ");");
            sleep(1);
        }
        LOG.info("Scroll Like User Done");
    }

    private void userSleep() {
        LOG.log(Level.INFO, "User Wait for [{0}] seconds", actionWaitTime);
        sleep(actionWaitTime);
    }

    private void sleep(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    private WebElement get(By by) {
        return driver.findElement(by);
    }

    private WebElement waitGet(By by) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    private int randomOf(int min, int max) {
        return new Random().nextInt(max - min + 1) + min;
    }

    private void logout() {
        LOG.log(Level.INFO, "Logging Out [{0}]", user.username);
        get(By.cssSelector(".logout")).click();
        sleep(2);
    }

    public void stop() {
        LOG.log(Level.INFO, "Stopping activity for user [{0}]", user.username);
        Optional.ofNullable(driver).ifPresent(WebDriver::quit);
    }
}
