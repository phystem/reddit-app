/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phystem.util;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTable;

/**
 *
 * @author Madhanraj
 */
public class ClipboardKeyAdapter extends KeyAdapter {

    private final JTable table;

    public ClipboardKeyAdapter(JTable table) {
        this.table = table;
    }

    @Override
    public void keyReleased(KeyEvent event) {
        if (event.isControlDown() || event.isMetaDown()) {
            switch (event.getKeyCode()) {
                case KeyEvent.VK_C:
                    cancelEditing();
                    JTableCCP.copyToClipboard(table, false);
                    break;
                case KeyEvent.VK_X:
                    cancelEditing();
                    JTableCCP.copyToClipboard(table, true);
                    break;
                case KeyEvent.VK_V:
                    cancelEditing();
                    JTableCCP.pasteFromClipboard(table);
                    break;
                default:
                    break;
            }
        }

    }

    private void cancelEditing() {
        if (table.getCellEditor() != null) {
            table.getCellEditor().cancelCellEditing();
        }
    }

}
