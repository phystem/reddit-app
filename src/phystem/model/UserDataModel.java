/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phystem.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Comparator.reverseOrder;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author Madhanraj
 */
public class UserDataModel extends AbstractTableModel {

    List<UserData> users = new ArrayList<>();

    String[] headers = new String[]{"UserName", "Password", "Proxy", "UserAgent", "WaitTime"};

    public UserDataModel() {
        load();
    }

    private void load() {
        try (Reader in = new FileReader("data/info.csv");) {
            loadFromRecords(in);
        } catch (IOException ex) {
            Logger.getLogger(UserDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadFromRecords(Reader in) {
        try {
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().withSkipHeaderRecord().withIgnoreEmptyLines().parse(in);
            for (CSVRecord record : records) {
                UserData uData = new UserData();
                uData.username = record.get("UserName");
                uData.password = record.get("Password");
                uData.proxy = record.get("Proxy");
                uData.uAgent = record.get("UserAgent");
                uData.waitTime = Integer.valueOf(record.get("WaitTime"));
                users.add(uData);
            }
        } catch (IOException ex) {
            Logger.getLogger(UserDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadFromText(String text) {
        text = Stream.of(headers).collect(Collectors.joining(",")) + "\n" + text;
        loadFromRecords(new StringReader(text));
        fireTableStructureChanged();
    }

    public List<UserData> getUsers() {
        return users;
    }

    @Override
    public int getRowCount() {
        return users.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 5:
                return "Status";
            default:
                return headers[columnIndex];
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return Object.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return users.get(rowIndex).username;
            case 1:
                return users.get(rowIndex).password;
            case 2:
                return users.get(rowIndex).proxy;
            case 3:
                return users.get(rowIndex).uAgent;
            case 4:
                return users.get(rowIndex).waitTime;
            case 5:
                return users.get(rowIndex).status;
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                users.get(rowIndex).username = Objects.toString(aValue, "");
                break;
            case 1:
                users.get(rowIndex).password = Objects.toString(aValue, "");
                break;
            case 2:
                users.get(rowIndex).proxy = Objects.toString(aValue, "");
                break;
            case 3:
                users.get(rowIndex).uAgent = Objects.toString(aValue, "");
                break;
            case 4:
                users.get(rowIndex).waitTime = Integer.valueOf(aValue.toString());
                break;
            case 5:
                users.get(rowIndex).status = Objects.toString(aValue, "");
                break;
        }
    }

    public void addNew() {       
        users.add(new UserData());
        fireTableRowsInserted(users.size(), users.size());
    }

    public void delete(int[] indices) {
        IntStream.of(indices).boxed().sorted(reverseOrder()).forEachOrdered((index) -> {
            fireTableRowsDeleted(index, index);
            users.remove(index.intValue());
        });
    }

    public void save() {
        try (FileWriter out = new FileWriter("data/info.csv");
                CSVPrinter printer = new CSVPrinter(out, CSVFormat.EXCEL.withIgnoreEmptyLines());) {
            printer.printRecord(Arrays.asList(headers));
            for (UserData user : users) {
                printer.printRecord(user.forSaving());
            }
        } catch (IOException ex) {
            Logger.getLogger(UserDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void clearRuns() {
        users.forEach(UserData::clearStatus);
        fireTableDataChanged();
    }
}
