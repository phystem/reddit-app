/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phystem.model;

/**
 *
 * @author Madhanraj
 */
public class UserData {

    public String username = "";
    public String password = "";

    public String proxy = "";

    public String uAgent = "";

    public int waitTime = 0;

    public String status = "";

    public Object[] forSaving() {
        return new Object[]{username, password, proxy, uAgent, waitTime};
    }

    public void clearStatus() {
        status = "";
    }

    public Boolean shouldRerun() {
        return status.equals("Run Failed") || status.equals("Proxy Failure") || status.equals("");
    }
}
